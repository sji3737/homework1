﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {


	public float distanceAway =5f;
	public float distanceUp = 3f;

	public Transform follow;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = follow.position + Vector3.up * distanceUp - Vector3.forward * distanceAway;
	}
}
